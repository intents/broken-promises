'use strict';
const env = process.env.NODE_ENV = 'test';
const chai = require('chai');

// Load Chai assertions
global.expect = chai.expect;
global.assert = chai.assert;
global.should = chai.should();
// Load Sinon
global.sinon = require('sinon');
// Initialize Chai plugins
chai.use(require('chai-properties'));
chai.use(require('chai-string'));
chai.use(require('sinon-chai'));
chai.use(require('chai-as-promised'));

// Load Chai assertions
global.expect = chai.expect;
global.assert = chai.assert;
global.should = chai.should();

