'use strict';

const request = require('supertest');
const server = require('../api');

describe('API Site', () => {
  it('should return 200 for index page', done => {
    request(server)
      .get('/')
      .expect(200, done);
  });
});

