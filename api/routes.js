'use strict';
const path = require('path');
const config = require('../config/environment');
const express = require('express');
const router = express.Router();

module.exports = (app, models) => {

  const getRoutes = routes => {
    // console.log('dir: ', __dirname);
    // console.log('routes: ', routes);
    let routePath = __dirname + '/' + routes;
    // console.log(routePath);
    return require(path.resolve(routePath))(app, models, router);
  };

  app.use(`api/remoteStorage`, getRoutes('remoteStorage/routes/index'));

  /* GET home page. */
  router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
  });

  return router;
};

