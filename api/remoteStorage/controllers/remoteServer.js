'use strict';

class RemoteServerController {

  static getServerInfo(req, res, next) {
    return res.json('ok');
  }

}

module.exports = RemoteServerController;

