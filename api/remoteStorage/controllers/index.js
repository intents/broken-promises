'use strict';

const RemoteDirectoryController = require('./remoteDirectory');
const RemoteFileController = require('./remoteFile');
const RemoteServerController = require('./remoteServer');
module.exports = {
  RemoteDirectoryController: RemoteDirectoryController,
  RemoteFileController: RemoteFileController,
  RemoteServerController: RemoteServerController
};
