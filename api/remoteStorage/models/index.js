'use strict';

const RemoteDirectory = require('./remoteDirectory');
const RemoteFile = require('./remoteFile');
const RemoteServer = require('./remoteServer');

module.exports = {
  RemoteDirectory: RemoteDirectory,
  RemoteFile: RemoteFile,
  RemoteServer: RemoteServer
};

