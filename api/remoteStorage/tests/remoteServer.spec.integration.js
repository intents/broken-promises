'use script';

const request = require('supertest');
const api = require('../../api');


describe('RemoteServer INTEGRATION Tests', () => {
  it('should respond with 200', done => {
    request(api)
      .get('/server/guid')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});

