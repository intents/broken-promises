'use strict';

const RemoteServerController = require('../controllers/remoteServer');


module.exports = (app, models, router) => {
  router.get('/server/:guid', RemoteServerController.getServerInfo);
  return router;
};

