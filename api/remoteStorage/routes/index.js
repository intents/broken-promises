'use strict';

module.exports = (app, models, router) => {
  const RemoteDirectoryRoutes = require('./remoteDirectory')(app, models, router);
  const RemoteFileRoutes = require('./remoteFile')(app, models, router);
  const RemoteServerRoutes = require('./remoteServer')(app, models, router);
  return router;
};

