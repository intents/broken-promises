'use strict';
const express = require('express');
const router = express.Router();
const config = require('../config/environment');
const path = require('path');

module.exports = (app) => {
  const getRoutes = routes => {
    return require(path.resolve(__dirname, '/../api/', routes));
  };

  app.use(`api/remoteStorage`, getRoutes('remoteStorage/routes'));

  /* GET home page. */
  router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
  });

};

